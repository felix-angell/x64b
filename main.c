#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

enum x64_reg {
	AL  = 0,  CL  = 1,  DL  = 2,  BL  = 3,  AH  = 4, CH  = 5, DH  = 6, BH  = 7,
	AX  = 0,  CX  = 1,  DX  = 2,  BX  = 3,  SP  = 4, BP  = 5, SI  = 6, DI  = 7,
	EAX = 0,  ECX = 1,  EDX = 2,  EBX = 3,  ESP = 4, EBP = 5, ESI = 6, EDI = 7,
	RAX = 0,  RCX = 1,  RDX = 2,  RBX = 3,  RSP = 4, RBP = 5, RSI = 6, RDI = 7,
	R8  = 8,  R9  = 9,  R10 = 10, 
	R11 = 11, R12 = 12, R13 = 13, R14 = 14, R15 = 15,

	XMM0 = 0, XMM1, XMM2, XMM3, XMM4, XMM5, XMM6, XMM7, XMM8,
	XMM9, XMM10, XMM11, XMM12, XMM13, XMM14, XMM15,
};

const char* reg_names[] = {
	"AX" ,  "CX",  "DX",  "BX",  "SP", "BP", "SI", "DI",
	"R8" ,  "R9",  "R10", "R11", "R12", "R13", "R14", "R15",
};

enum x64_instr {
	ADD = 0x83,
	SUB = 0x28,
	MOV = 0x89,
	RET = 0xc3,

	PUSH = 0x50,
	POP = 0x58,

	SEGCS = 0x2e,
	NOP = SEGCS,

	CALL = 0xe8,
	JMP = 0xe9,
	JMPS = 0xeb,

	LEA = 0x8d,

	JC = 0x72,
	JB = 0x72,
	JE = 0x74,
	JNE = 0x75,
	JL = 0x7c,
	JGE = 0x7d,
	JLE = 0x7e,
	JG = 0x7f,
};

void* 
alloc_emem(size_t size) {
	printf("allocating mmap page of size %ld\n", size);

	void* data = mmap(0, size,
		PROT_READ | PROT_WRITE,
		MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	
	if (data == (void*)-1) {
		perror("mmap");
		return NULL;
	}
	
	return data;
}

#define DEFAULT_PAGE_SIZE 32

struct x64_code {
	void* mem;
	size_t index;
	size_t capacity;
};

bool
init_x64_code(struct x64_code* code) {
	code->capacity = DEFAULT_PAGE_SIZE;
	code->mem = malloc(sizeof(*code->mem) * code->capacity);
	code->index = 0;
}

void*
load_x64(struct x64_code* code) {
	void* mem = alloc_emem(code->index);
	memcpy(mem, code->mem, code->index);
	if (mprotect(mem, code->index, PROT_READ | PROT_EXEC) == -1) {
		perror("mprotect");
		return NULL;
	}
	return mem;
}

void
emit_x64_code(struct x64_code* code, uint8_t* instr_code, size_t len) {
	if (code->index + len >= code->capacity) {
		code->capacity *= 2;
		code->mem = realloc(code->mem, sizeof(*code->mem) * code->capacity);
	}

#ifdef DEBUG_MODE
	printf("copied over %ld bytes\n", len);
	for (int i = 0; i < len; i++) {
		if (i > 0) printf(" ");
		printf("0x%02x", instr_code[i]);
	}
	printf("\n");
#endif

	uint8_t* codes = (uint8_t*)(code->mem);
	memcpy(&codes[code->index], instr_code, len);
	code->index += len;
}

uint8_t
encode_rex(bool is_64bit, uint8_t ext_sib_idx, uint8_t ext_modrm_reg, uint8_t ext_modrm_rm) {
	struct rex {
		uint8_t b : 1;
		uint8_t x : 1;
		uint8_t r : 1;
		uint8_t w : 1;
		uint8_t f : 4;
	};

	struct rex r = {
		.b = ext_modrm_rm,
		.x = ext_modrm_reg,
		.r = ext_sib_idx,
		.w = is_64bit,
		.f = 0b100,
	};
	return *((uint8_t*)(&r));
}

uint8_t encode_modrm(uint8_t mod, uint8_t rm, uint8_t reg) {
	assert(reg < R8 && rm < R8);
	struct mod_rm {
		// specifies a direct or indirect operand, optionally with
		// a displacement
		uint8_t rm : 3;

		// 3 bit opcode ext -> purely for distuingishing from other
		// 										 instructions
		// 3 bit register reference -> can be used as the source or
		// 										the dest of an instruction
		uint8_t reg : 3;

		// 0b11 -> register-direct addr mode is used
		// otherwise register indirect addr mode is used.
		uint8_t mod : 2;
	};

	struct mod_rm m;
	m.rm = rm;
	m.reg = reg;
	m.mod = mod;
	return *((uint8_t*)(&m));
}

uint8_t encode_disp8(uint64_t value) {
	assert(value <= 0xff);
	return (uint8_t)(value);
}

void 
emit_movrr(struct x64_code* code, enum x64_reg src, enum x64_reg dest) {
	printf("mov %s, %s\n", reg_names[src], reg_names[dest]);

	uint8_t encoded[3] = {
		encode_rex(true, 0, 0, 0),
		MOV,
		encode_modrm(3, dest, src),
	};
	
	emit_x64_code(code, &encoded[0], sizeof(encoded));
}

void
emit_addir(struct x64_code* code, uint8_t val, enum x64_reg dest) {
	printf("add %d, %s\n", val, reg_names[dest]);
	
	uint8_t encoded[4] = {
		encode_rex(true, 0, 0, 0),
		ADD,
		encode_modrm(0b11, 0, 0),
		encode_disp8(val),
	};
	
	emit_x64_code(code, &encoded[0], sizeof(encoded));
}

void emit_pushr(struct x64_code* code, enum x64_reg reg) {
	printf("push %s\n", reg_names[reg]);
	uint8_t encoded[1] = { PUSH + reg };
	emit_x64_code(code, &encoded[0], sizeof(encoded));
}

void emit_popr(struct x64_code* code, enum x64_reg reg) {
	printf("pop %s\n", reg_names[reg]);
	uint8_t encoded[1] = { POP + reg };
	emit_x64_code(code, &encoded[0], sizeof(encoded));
}

void
emit_ret(struct x64_code* code) {
	printf("ret\n");

	uint8_t encoded[1] = {
		RET,
	};
	emit_x64_code(code, &encoded[0], sizeof(encoded));
}

typedef uint64_t (*x64_func)(uint64_t);

int main(int argc, char* argv[]) {
	struct x64_code code;
	init_x64_code(&code);

	emit_movrr(&code, RDI, RAX);
	emit_addir(&code, 125, RAX);
	emit_ret(&code);

	void* mem = load_x64(&code);
	if (mem == NULL) {
		return -1;
	}

	x64_func f = mem;
	int res = f(2);
	printf("result is %d\n", res);
}