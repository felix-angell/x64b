src = $(wildcard *.c)
obj = $(patsubst %.c,%.o,$(src))
out := main

CCFLAGS = -Wall -Wextra -g3 -std=c99

default: $(out)

%.o: %.c
	$(CC) -c $< -o $@

$(out): $(obj)
	$(CC) $(LDFLAGS) $(obj) -o $@
